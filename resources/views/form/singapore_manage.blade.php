@extends('home.home_content')
@section('main_content')
<div style="margin-top:100px;"></div>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr>
         	<th class="text-center">Seafarer's Name</th>
    			<th class="text-center">Gender</th>
    			<th class="text-center">Date of birth</th>
                <th class="text-center">Place of birth</th>
                <th class="text-center">Nationality</th>
    			<th class="text-center">Dept</th>
    			<th class="text-center">Note</th>
        </tr>
      </thead>
      <tbody>
        @foreach( $datas as $data )
      	<tr>
          	<td>{{$data->seafarers_name}}</td>
          	<td>{{$data->gender}}</td>
            <td>{{$data->date_of_birth}}</td>
          	<td>{{$data->place_of_birth}}</td>
            <td>{{$data->nationality}}</td>
        	<td>{{$data->dept}}</td>
          <td>
          	<a href="{{route('forms.download',$data->id)}}" class="btn btn-primary" title="Download">                                <svg width="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">                                    <path d="M12.1221 15.436L12.1221 3.39502" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>                                    <path d="M15.0381 12.5083L12.1221 15.4363L9.20609 12.5083" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>                                    <path d="M16.7551 8.12793H17.6881C19.7231 8.12793 21.3721 9.77693 21.3721 11.8129V16.6969C21.3721 18.7269 19.7271 20.3719 17.6971 20.3719L6.55707 20.3719C4.52207 20.3719 2.87207 18.7219 2.87207 16.6869V11.8019C2.87207 9.77293 4.51807 8.12793 6.54707 8.12793L7.48907 8.12793" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>                                </svg>                            </i></a>
          </td>
          </tr>
          @endforeach
      </tbody>
    </table>
@endsection