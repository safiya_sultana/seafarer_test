<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Singapore;

class SingapurFormController extends Controller
{
    public function index(){
    	return view('form.singapore_new');
    }
    public function save(Request $request){
        $data = new Singapore;
        $data->seafarers_name=$request->seafarers_name;
        $data->gender=$request->gender;
        $data->date_of_birth=$request->date_of_birth;
        $data->place_of_birth=$request->place_of_birth;
        $data->nationality=$request->nationality;
        $data->id_passport_no=$request->id_passport_no;
        $data->dept=$request->dept;
        $data->rank=$request->rank;
        $data->home_address=$request->home_address;
        $data->routine_duties=$request->routine_duties;
        $data->trading_area=$request->trading_area;
        
        $data->eye_problem=$request->eye_problem;
        $data->high_blood_pressure=$request->high_blood_pressure;
        $data->heart_disease=$request->heart_disease;
        $data->heart_surgery=$request->heart_surgery;
        $data->varicose_veins=$request->varicose_veins;
        $data->asthma_bronchitis=$request->asthma_bronchitis;
        $data->blood_disorder=$request->blood_disorder;
        $data->diabetes=$request->diabetes;
        $data->thyroid_problem=$request->thyroid_problem;
        $data->digestive_disorder=$request->digestive_disorder;
        $data->kidney_problem=$request->kidney_problem;
        $data->skin_Problem=$request->skin_Problem;
        $data->allergies=$request->allergies;
        $data->infectious_diseases=$request->infectious_diseases;
        $data->hernia=$request->hernia;
        $data->genital_disorder=$request->genital_disorder;
        $data->pregnancy=$request->pregnancy;
        $data->sleep_problem=$request->sleep_problem;
        $data->smoke_alcohol_drug=$request->smoke_alcohol_drug;
        $data->operation=$request->operation;
        $data->epilesy_seizures=$request->epilesy_seizures;
        $data->dizziness=$request->dizziness;
        $data->loss_of_consciousness=$request->loss_of_consciousness;
        $data->psychiatric_problem=$request->psychiatric_problem;
        $data->depression=$request->depression;
        $data->attempted_suicide=$request->attempted_suicide;
        $data->loss_of_memory=$request->loss_of_memory;
        $data->balance_problem=$request->balance_problem;
        $data->severe_headache=$request->severe_headache;
        $data->hearing_throat_problem=$request->hearing_throat_problem;
        $data->restricted_mobility=$request->restricted_mobility;
        $data->joint_problem=$request->joint_problem;
        $data->amputation=$request->amputation;
        $data->fracture=$request->fracture;
        
        $data->provide_details=$request->provide_details;
        
        $data->save();
        return redirect('/forms/new')->with('Data inserted successfully');
    }
    
    public function manage(){
        $datas = Singapore::all();
        return view('form.singapore_manage', ['datas'=>$datas]);
    }
}
