<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSingaporesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('singapores', function (Blueprint $table) {
            $table->id('id');
            $table->string('seafarers_name');
            $table->string('gender');
            $table->date('date_of_birth')->format('d.m.Y');
            $table->string('place_of_birth');
            $table->string('nationality');
            $table->string('id_passport_no');
            $table->string('dept');
            $table->string('rank');
            $table->string('home_address');
            $table->string('routine_duties');
            $table->string('trading_area');
            
            $table->string('eye_problem');
            $table->string('high_blood_pressure');
            $table->string('heart_disease');
            $table->string('heart_surgery');
            $table->string('varicose_veins');
            $table->string('asthma_bronchitis');
            $table->string('blood_disorder');
            $table->string('diabetes');
            $table->string('thyroid_problem');
            $table->string('digestive_disorder');
            $table->string('kidney_problem');
            $table->string('skin_Problem');
            $table->string('allergies');
            $table->string('infectious_diseases');
            $table->string('hernia');
            $table->string('genital_disorder');
            $table->string('pregnancy');
            $table->string('sleep_problem');
            $table->string('smoke_alcohol_drug');
            $table->string('operation');
            $table->string('epilesy_seizures');
            $table->string('dizziness');
            $table->string('loss_of_consciousness');
            $table->string('psychiatric_problem');
            $table->string('depression');
            $table->string('attempted_suicide');
            $table->string('loss_of_memory');
            $table->string('balance_problem');
            $table->string('severe_headache');
            $table->string('hearing_throat_problem');
            $table->string('restricted_mobility');
            $table->string('joint_problem');
            $table->string('amputation');
            $table->string('fracture');
            
            $table->string('provide_details');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('singapores');
    }
}
