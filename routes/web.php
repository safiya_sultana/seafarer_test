<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SigninController;
use App\Http\Controllers\SingapurFormController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.sign_in');
});
Route::get('/dashboard', [SigninController::class, 'dashboard'])->middleware(['auth'])->name('dashboard');
;
require __DIR__.'/auth.php';

  // Forms Route Start
Route::get('/forms/new', [SingapurFormController::class, 'index'])->name('forms.new');
Route::post('/forms/save', [SingapurFormController::class, 'save'])->name('forms.save');
Route::get('/forms/manage', [SingapurFormController::class, 'manage'])->name('forms.manage');
Route::get('/forms/download/{id}', [SingapurFormController::class, 'pdf'])->name('forms.download');